import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class TestFibonacciSequence {
    @Test
    public void testGetTerm(){
        FibonacciSequence f = new FibonacciSequence(9, 7);
        int n=6;
        assertEquals(62, f.getTerm(n)); 
    }
    @Test 
    public void testNegativeN(){
        FibonacciSequence f = new FibonacciSequence(2, 5);
        int n=-2;
        try {
            f.getTerm(n);
            fail("No data validation!");
        } catch (IllegalArgumentException e) {
            /*If the code throws an exception it will pass */
        }
    }
}
