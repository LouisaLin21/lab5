public class SubtractonacciSequence extends Sequence{
    private int firstNum;
    private int secondNum;
    public SubtractonacciSequence(int n1, int n2){
        this.firstNum=n1;
        this.secondNum=n2;
    }
    @Override
    public int getTerm(int n){
        //data validation
        int n3=0;
        if(n<=0) {
            throw new IllegalArgumentException("example");
        }
        else if(n==1){
            n3=this.firstNum;
            System.out.println("the result is "+n3);
        }
        else if(n==2){
            n3=this.secondNum;
            System.out.println("the result is "+n3);
        }
        else{
            int n1=this.firstNum;
            int n2=this.secondNum;
            for(int i=0; i<n-2;i++){
              n3=n1-n2;
              n1=n2;
              n2=n3;
              System.out.println("the result is "+n3);
            }
            }
       return n3;
    }
    public static void main(String[]args){
        SubtractonacciSequence t = new SubtractonacciSequence(5, 6);
        t.getTerm(1);
    }
}
