//Qiuning Liang 2037000
public class FibonacciSequence extends Sequence{
    private int first;
    private int seconde;

    public FibonacciSequence(int first, int seconde){
        this.first = first;
        this.seconde = seconde;
    }

    int nthnumber = 0;
    public int getTerm(int n){
        if (n < 0){
        throw new IllegalArgumentException("example");
        }
        else if (n==1){
            return this.first;
        }
        else if (n==2){
            return this.seconde;
        }
        else
        {
        int first = this.first;
        int seco = this.seconde;
        int thrd = 0;
        int i = 0;
        while ( i < n-2){
            thrd = first + seco;
            first = seco;
            seco = thrd;
            i++;
        }
        return thrd;
        }
    }
}

