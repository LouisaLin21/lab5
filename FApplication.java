import java.util.*;

public class FApplication {
    public static void main(String[]args){
        Scanner reader = new Scanner(System.in);
        System.out.println("please input your numbers");
        String userInput= reader.next();
        Sequence[] answer = parse(userInput);
        print(answer,10);
    }

    public static void print(Sequence[] s,int n){
        //The method should print the first n terms of ALL the sequences within the Sequence[]. 
        for (int i = 0; i < s.length; i++) {
            for (int j = 1; j <=n; j++) {
                int total=s[i].getTerm(j);
                System.out.print(total+", ");     
            }
               System.out.print("\n");  
       }
    }

// Qiuning Liang 2037000
    public static Sequence[] parse(String s){
        String[] a = s.split(";");
        Sequence[] seq = new Sequence[a.length/4];
        // int typeIndex=0;
        for(int i = 0; i < seq.length; i ++){
            //If it is fib 
            if (a[4*i].equals("Fib")) {
                int fir = Integer.parseInt(a[4*i+1]);
                System.out.println(fir);
                int sec = Integer.parseInt(a[4*i+2]);
                System.out.println(sec);
                seq[i] = new FibonacciSequence(fir, sec);
            } 
            //else, If it is sub
            else {
                int fir = Integer.parseInt(a[4*i+1]);
                int sec = Integer.parseInt(a[4*i+2]);
                seq[i] = new SubtractonacciSequence(fir, sec);
            }
        }
           return seq;
    }
}

